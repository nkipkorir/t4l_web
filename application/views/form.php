<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>T4L | Donor </title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
        <!-- iCheck for checkboxes and radio inputs -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/all.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="../../index2.html" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>T4</b>L</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>T4L</b></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="#">Dashboard</a></li>
                                <li><a href="#about">Blood Test Results</a></li>
                                <li><a href="#contact">Sms Campaign</a></li>
                                <li><a href="#contact">Inbox</a></li>
                                <li><a href="#contact">Sent Messages</a></li>
                                <li><a href="#contact">Emergency Appeal</a></li>
                                <li><a href="#contact">Reference Data</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reports,Admin options <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Reports</a></li>


                                        <li role="separator" class="divider"></li>

                                        <li><a href="#">Sign Out</a></li> 
                                    </ul>     


                                </li>

                                </li>
                            </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">

                    </div>
                    <!-- search form -->
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="../../index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                                <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-files-o"></i>
                                <span>Layout Options</span>
                                <span class="label label-primary pull-right">4</span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="../layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
                                <li><a href="../layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
                                <li><a href="../layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
                                <li><a href="../layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="../widgets.html">
                                <i class="fa fa-th"></i> <span>Widgets</span> <small class="label pull-right bg-green">new</small>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-pie-chart"></i>
                                <span>Charts</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="../charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
                                <li><a href="../charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
                                <li><a href="../charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
                                <li><a href="../charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-laptop"></i>
                                <span>UI Elements</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="../UI/general.html"><i class="fa fa-circle-o"></i> General</a></li>
                                <li><a href="../UI/icons.html"><i class="fa fa-circle-o"></i> Icons</a></li>
                                <li><a href="../UI/buttons.html"><i class="fa fa-circle-o"></i> Buttons</a></li>
                                <li><a href="../UI/sliders.html"><i class="fa fa-circle-o"></i> Sliders</a></li>
                                <li><a href="../UI/timeline.html"><i class="fa fa-circle-o"></i> Timeline</a></li>
                                <li><a href="../UI/modals.html"><i class="fa fa-circle-o"></i> Modals</a></li>
                            </ul>
                        </li>
                        <li class="treeview active">
                            <a href="#">
                                <i class="fa fa-edit"></i> <span>Forms</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="active"><a href="general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>
                                <li><a href="advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
                                <li><a href="editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-table"></i> <span>Tables</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="../tables/simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
                                <li><a href="../tables/data.php"><i class="fa fa-circle-o"></i> Data tables</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="../calendar.html">
                                <i class="fa fa-calendar"></i> <span>Calendar</span>
                                <small class="label pull-right bg-red">3</small>
                            </a>
                        </li>
                        <li>
                            <a href="../mailbox/mailbox.html">
                                <i class="fa fa-envelope"></i> <span>Mailbox</span>
                                <small class="label pull-right bg-yellow">12</small>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-folder"></i> <span>Examples</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="../examples/invoice.html"><i class="fa fa-circle-o"></i> Invoice</a></li>
                                <li><a href="../examples/profile.html"><i class="fa fa-circle-o"></i> Profile</a></li>
                                <li><a href="../examples/login.html"><i class="fa fa-circle-o"></i> Login</a></li>
                                <li><a href="../examples/register.html"><i class="fa fa-circle-o"></i> Register</a></li>
                                <li><a href="../examples/lockscreen.html"><i class="fa fa-circle-o"></i> Lockscreen</a></li>
                                <li><a href="../examples/404.html"><i class="fa fa-circle-o"></i> 404 Error</a></li>
                                <li><a href="../examples/500.html"><i class="fa fa-circle-o"></i> 500 Error</a></li>
                                <li><a href="../examples/blank.html"><i class="fa fa-circle-o"></i> Blank Page</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-share"></i> <span>Multilevel</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                                <li>
                                    <a href="#"><i class="fa fa-circle-o"></i> Level One <i class="fa fa-angle-left pull-right"></i></a>
                                    <ul class="treeview-menu">
                                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                                        <li>
                                            <a href="#"><i class="fa fa-circle-o"></i> Level Two <i class="fa fa-angle-left pull-right"></i></a>
                                            <ul class="treeview-menu">
                                                <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                                                <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                            </ul>
                        </li>
                        <li><a href="../../documentation/index.html"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
                        <li class="header">LABELS</li>
                        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
                        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
                        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>

                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Forms</a></li>
                        <li class="active">Reports</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <!--   --> <h3 class="box-title">Donation Search</h3>
                                </div><!-- /.box-header -->
                                <!-- Donation search Form  -->
                                <form role="form">
                                    <div class="box-body">

                                        <div class="checkbox">
                                            <label>Search By:Cell Phone Number
                                                <input type="radio" class="flat-red" name="donor">
                                            </label>
                                            <label>Donor Number
                                                <input type="radio" class="flat-red" name="donor">
                                            </label>
                                            <label>ID Number
                                                <input type="radio" class="flat-red" name="donor">
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label for="GG"></label>
                                            <input type="text" class="form-control" id="donordetails" placeholder="Enter Value ...">
                                        </div>

                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary btn-sm pull-left"><i class=" fa fa-search"></i>Search</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->

                            <!-- Form Element sizes -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        <div class="col-md-12">
                            <!-- Horizontal Form -->
                            <div class="box box-info">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Enrol New Donor</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <form class="form-horizontal enroll_new_donor_form" id="enroll_new_donor_form" >
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="Surname" class="col-sm-2 control-label">Surname</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="surname" name="surname" placeholder="Surname">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="OtherNames" class="col-sm-2 control-label">Other Names</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="othernames" name="othernames" placeholder="Other Names">
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="studentnationalid" class="col-sm-2 control-label">Student / National Id</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control national_id" id="national_id" name="national_id" placeholder="Student / National Id">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="gender" class="col-sm-2 control-label">Gender : </label>
                                            <div class="col-sm-10">
                                                <select name="gender" id="gender" class="gender form-control">
                                                    <option value="">Select Gender</option>
                                                    <option value="male">Male</option>
                                                    <option value="female">Female</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="bloodgroup" class="col-sm-2 control-label">Blood Group</label>
                                            <div class="col-sm-10">
                                                <select name="blood_group" id="blood_group" class="form-control blood_group">
                                                    <option value="Do not Know">Do not know</option>
                                                    <option value="A+">A+</option>
                                                    <option value="A-">A-</option>
                                                    <option value="B+">B+</option>
                                                    <option value="B-">B-</option>
                                                    <option value="AB+">AB+</option>
                                                    <option value="AB-">AB-</option>
                                                    <option value="O+">O+</option>
                                                    <option value="O-">O-</option>
                                                </select>
                                            </div>
                                        </div>


                                        <!-- textarea -->                 
                                        <div class="form-group">
                                            <label for="contact_details" class="col-sm-2 control-label">Contact Details (Postal Address)</label>
                                            <div class="col-sm-10">
                                                <!--<label>Textarea</label>-->
                                                <textarea class="form-control" id="contact_details" name="contact_details" rows="3" placeholder="Contact Details (Postal Address)"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="cell_phone" class="col-sm-2 control-label">Cell Phone Number</label>
                                            <div class="col-sm-10">
                                                <input type="number" name="cell_phone" class="form-control" id="cell_phone" placeholder="Cell Phone Number">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="home_phone" class="col-sm-2 control-label">Home Phone Number</label>
                                            <div class="col-sm-10">
                                                <input type="number" name="home_phone" class="form-control" id="home_phone" placeholder="Home Phone Number">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="dateofbirth" class="col-sm-2 control-label">Date Of Birth</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="dob" class="form-control dob" id="calendar" placeholder="Date Of Birth">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="email" class="col-sm-2 control-label">Email</label>
                                            <div class="col-sm-10">
                                                <input type="email" name="email" class="form-control" id="email" placeholder="Email">
                                            </div>
                                        </div>                   



                                        <div class="form-group">
                                            <label for="county" class="col-sm-2 control-label">County Of Residence</label>
                                            <div class="col-sm-10">
                                                <select name="county_residence" id="county_residence" class="form-control">                        
                                                    <option value="baringo">Baringo</option>
                                                    <option value="bomet">Bomet</option>
                                                    <option value="bungoma">Bungoma</option>
                                                    <option value="busia">Busia</option>
                                                    <option value="elgeyo_marakwet">Elgeyo Marakwet</option>                                               
                                                </select>
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <label for="levelofeducation" class="col-sm-2 control-label">Level Of Education</label>
                                            <div class="col-sm-10">
                                                <select name="education_level" id="education_level" class=" education_level form-control">                        
                                                    <option value="none">None</option>
                                                    <option value="primary">Primary</option>
                                                    <option value="secondary">Secondary</option>
                                                    <option value="tertiary">Tertiary</option>
                                                    <option value="university">University</option>
                                                    <!-- <option value="B+">Audi</option> -->
                                                </select>
                                            </div>
                                        </div> 

                                        <div class="form-group">
                                            <label for="maritalstatus" class="col-sm-2 control-label">Marital Status</label>
                                            <div class="col-sm-10" class="form-control">
                                                <select name="marital_status" id="marital_status" class=" marital_status form-control">                        
                                                    <option value="Single">Single</option>
                                                    <option value="Married">Married</option>
                                                    <option value="Divorced/Separated/">Divorced/Separated</option>
                                                    <option value="Widowed">Widowed</option>                        
                                                </select>
                                            </div>

                                        </div><div class="form-group">
                                            <label for="recruiter" class="col-sm-2 control-label">Recruiter/ Mobilizer</label>
                                            <div class="col-sm-10">
                                                <select name="recruiter" id="recruiter" class=" recruiter form-control">                        
                                                    <option value="Nairobi RBTC">Nairobi RBTC</option>
                                                    <option value="Embu RBTC">Embu RBTC</option>
                                                    <option value="Nakuru RBTC">Nakuru RBTC</option>
                                                    <option value="Kisumu RBTC">MACHAKOS</option>
                                                    <option value="Eldoret RBTC">Eldoret RBTC</option>                        
                                                    <option value="Kakamega Satellite">Kakamega Satellite</option>
                                                    <option value="Kisii Satellite">Kisii Satellite</option>
                                                    <option value="Kericho Satellite">Kericho Satellite</option>
                                                    <option value="Naivasha Satellite">Naivasha Satellit</option>
                                                    <option value="Meru Satellite">Meru Satellite</option>
                                                    <option value="Nyeri Satellite">Nyeri Satellite</option>
                                                    <option value="Garissa Satellite">Garissa Satellite</option>
                                                    <option value="Voi Satellite">Voi Satellite</option>
                                                    <option value="Machakos Satellite">Machakos Satellite</option>
                                                    <option value="Thika Satellite">Thika Satellite</option>                        
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="othermobilisingpartner"  class="col-sm-2 control-label">Other Mobilisisng Partner</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="othermobilisingpartner" name="other_recruiter" placeholder= "Other Mobilisisng Partner">
                                            </div>
                                        </div>  




                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="Yes" name="accept_consent" id="accept_consent" class="flat-red accept_consent">I hereby give consent to NBTS to send me SMS texts
                                                    </label>
                                                </div>
                                            </div>




                                        </div>
                                    </div><!-- /.box-body -->
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary btn-sm pull-left ion-checkmark">Create</button>
                                        <button type="button" class="btn btn-danger btn-sm pull-right ion-minus-circled">Cancel</button>
                                    </div><!-- /.box-footer -->
                                </form>
                            </div><!-- /.box -->
                            <!-- general form elements disabled -->


                            <!-- /.box-body -->
                        </div><!-- /.box -->

                    </div><!--/.col (right) -->       


                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">CREATE DONOR/HEALTH QUESTIONNAIRE</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <form class="form-horizontal enroll_new_donor_form" id="enroll_new_donor_form" >
                            <div class="box-body"> 



                                <div class="form-group">
                                    <label for="home_phone" class="col-sm-10">Last donation date</label>
                                    <div class="col-sm-2">
                                        <input type="text" name="lst_dntn_dte" class="lst_dntn_dte form-control" id="lst_dntn_dte" placeholder="Last Donation Date">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="home_phone" class="col-sm-10">Donation Date</label>
                                    <div class="col-sm-2">
                                        <input type="text" name="dntn_dte" class="dntn_dte form-control" id="dntn_dte" placeholder="Donation Date">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="home_phone" class="col-sm-10">Donation Number</label>
                                    <div class="col-sm-2">
                                        <input type="number" name="dntn_no" class="dntn_no form-control" id="dntn_no" placeholder="Donation Number">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="home_phone" class="col-sm-10">Recruiter/Mobilizer</label>
                                    <div class="col-sm-2">
                                        <input type="search" name="recruiter" class="recruiter form-control" id="recruiter" placeholder="Recruiter/Mobilizer">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <h4 class="col-sm-12"> <b> Health Questionnaire</b></h4>
                                </div>


                                <div class="form-group">
                                    <label for="H1" class="col-sm-10">1.Are you feeling well and in good health today? : </label>
                                    <div class="col-sm-2 ">
                                        <select name="H1" id="H1" class="H1 form-control gender">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="H2" class="col-sm-10">2.Have you eaten in the last 6 hours</label>
                                    <div class="col-sm-2 ">
                                        <select name="H2" id="H2" class=" H2 form-control gender">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="H3" class="col-sm-10">3.Have you ever fainted</label>
                                    <div class="col-sm-2 ">
                                        <select name="H3" id="H3" class="H3 form-control gender">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <h5 class="col-sm-12"> In the past 6 months have you : </h5>
                                </div>

                                <div class="form-group">
                                    <label for="H4" class="col-sm-10">4.Been ill, received any treatment or any medication?</label>
                                    <div class="col-sm-2 ">
                                        <select name="H4" id="H4" class="H4 form-control gender">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="H5" class="col-sm-10">5.Had any injection or vaccination(immunization)?</label>
                                    <div class="col-sm-2 ">
                                        <select name="H5" id="H5" class="H5 form-control gender">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="H6" class="col-sm-10">6.Female donors,Have you been pregnant or breast feeding?</label>
                                    <div class="col-sm-2 ">
                                        <select name="H6" id="H6" class="H6 form-control gender">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <h5 class="col-sm-12">In the past 12 months have you : </h5>
                                </div>




                                <div class="form-group">
                                    <label for="H7" class="col-sm-10">7.Received a blood transfusion or any blood products?</label>
                                    <div class="col-sm-2 ">
                                        <select name="H7" id="H7" class="H7 form-control gender">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <h5 class="col-sm-12">Do you have or ever had :</h5>
                                </div>


                                <div class="form-group">
                                    <label for="H8" class="col-sm-10">8. Any problems with heart or lungs e.g asthma?</label>
                                    <div class="col-sm-2 ">
                                        <select name="H8" id="H8" class="H8 form-control gender">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="H9" class="col-sm-10">9.A bleeding condition or blood disease?</label>
                                    <div class="col-sm-2 ">
                                        <select name="H9" id="H9" class="H9 form-control gender">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="H10" class="col-sm-10">10.Any type of cancer?</label>
                                    <div class="col-sm-2 ">
                                        <select name="H10" id="H10" class="H10 form-control gender">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="H11" class="col-sm-10">11.Diabetes,epilepsy or TB?</label>
                                    <div class="col-sm-2 ">
                                        <select name="H11" id="H11" class="H11 form-control gender">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="H12" class="col-sm-10">12.Any other long term illness. Please specify?</label>
                                    <div class="col-sm-2 ">
                                        <select name="H12" id="H12" class="H12 form-control gender">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group H12_Yes_Div" style="display: none;">
                                    <label for="H12_illness" class="col-sm-10">Please Specify : ?</label>
                                    <div class="col-sm-2 ">
                                        <textarea class="form-control H12_illness" name="H12_illness" id="H12_illness"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <h5 class="col-sm-12">The lives of the patient who receive your blood are totally 
                                        dependant on your honesty in answering the questions below.
                                        Your answers will be treated 
                                        in a confidential manner</h5>
                                </div>

                                <div class="form-group">
                                    <h5 class="col-sm-12">In the past 12 months have you :</h5>
                                </div>

                                <div class="form-group">
                                    <label for="R1" class="col-sm-10">1.Received or given money, goods or favours in exchange for sexual activities?</label>
                                    <div class="col-sm-2 ">
                                        <select name="R1" id="R1" class="form-control R1">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="R2" class="col-sm-10">2.Had sexual activity with a person whose background you do ot know?</label>
                                    <div class="col-sm-2 ">
                                        <select name="R2" id="R2" class="form-control R2">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="R3" class="col-sm-10">3.Been raped or sodomised?</label>
                                    <div class="col-sm-2 ">
                                        <select name="R3" id="R3" class="form-control R3">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="R4" class="col-sm-10">4.Had a stab wound or had an 
                                        accident needle stick injury e.g injection?</label>
                                    <div class="col-sm-2 ">
                                        <select name="R4" id="gender" class="form-control R4">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="R5" class="col-sm-10">5.Had any tatooing or body piercing e.g ear piercing?</label>
                                    <div class="col-sm-2 ">
                                        <select name="R5" id="R5" class="R5 form-control R5">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="R6" class="col-sm-10">6.Had a sexually transmitted disease(STD)?</label>
                                    <div class="col-sm-2 ">
                                        <select name="R6" id="R6" class="R6 form-control R6">
                                            <option value="Yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="R7" class="col-sm-10">7.Live with or had sexual contact 
                                        with someone with yellow eyes or yellow skin?</label>
                                    <div class="col-sm-2 ">
                                        <select name="R7" id="R7" class="form-control R7">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="R8" class="col-sm-10">8.Had sexual activity with anyone besides your regular sex partner?</label>
                                    <div class="col-sm-2 ">
                                        <select name="R8" id="R8" class="form-control R8">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <h5 class="col-sm-12">Have you ever</h5>
                                </div>


                                <div class="form-group">
                                    <label for="R9" class="col-sm-10">9.Had yellow eyes or yellow skin?</label>
                                    <div class="col-sm-2 ">
                                        <select name="R9" id="R9" class="form-control R9">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="R10" class="col-sm-10">10.Injected yourself or been injected, besides in a health facility?</label>
                                    <div class="col-sm-2 ">
                                        <select name="R10" id="R10" class="form-control R10">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="R11" class="col-sm-10">11.Used non medical drugs such as Marijuana,Cocaine etc?</label>
                                    <div class="col-sm-2 ">
                                        <select name="R11" id="R11" class="form-control R11">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="R12" class="col-sm-10">12.Have you or your partner been tested for HIV?</label>
                                    <div class="col-sm-2 ">
                                        <select name="R12" id="R12" class="form-control R12">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="R13" class="col-sm-10">13.Do you consider your blood safe to transfuse to a patient?</label>
                                    <div class="col-sm-2 ">
                                        <select R13="R13" id="R13" class="form-control R13">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <b><h5 class="col-sm-12"><p>DECLARATION</p></b>
                                    <p>I declare that the information I have given above is correct
                                        I understand that my blood will be tested for HIV, Hepatitis B & C, and syphilis and the results of my 
                                        tests may be obtained from the National Blood Transfusion service.
                                        I understand that the Kenya National Blood Transfusion Service may use any communication medium(s)
                                        to send me important information. Such medium(s) shall include but not limited to e-mail,post office,mobile telephoned
                                        and/or fixed telephone. I hereby give consent to KNBTS to use the contact details provided in this form to communicate 
                                        to me as the need may be.</p>
                                    <p></p>
                                    </h5>
                                </div>

                                <div class="form-group">
                                    <label for="weight" class="col-sm-10">Weight(kgs)</label>
                                    <div class="col-sm-2">
                                        <label>
                                            <input type="text" name="weight" id="weight" class="weight form-control" placeholder="Weight(kgs)">
                                        </label>
                                    </div>                                                                            
                                </div>
                                <div class="form-group">
                                    <label for="hb" class="col-sm-10">Hb >12.5g/dl</label>
                                    <div class="col-sm-2">
                                        <label>
                                            <input type="text" name="hb" id="hb" class="haemoglobin form-control" placeholder="Haemoglogin >12.5">
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="bp" class="col-sm-7">BP</label>
                                    <div class="col-sm-2">
                                        <label>
                                            <input type="text" name="systolic_range" id="systolic_range" class="systolic_range form-control" placeholder="Systolic Range is 50-210">
                                        </label>
                                    </div>
                                    <div class="col-sm-1">
                                        <h5>/</h5>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>
                                            <input type="text" name="diastolic_range" id="diastolic_range" class="diastolic_range form-control" placeholder="Diastolic Range is 33-120">                                            
                                        </label>
                                    </div>                                    
                                </div>
                                <div class="form-group">
                                    <label for="pulse" class="col-sm-10">Pulse</label>
                                    <div class="col-sm-2">
                                        <label>
                                            <input type="text" name="pulse" id="pulse" class="pulse form-control" placeholder="Pusle">
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donor_accepted" class="col-sm-10">Volume</label>
                                    <div class="col-sm-2">
                                        <label>
                                            <input type="text" name="volume" id="volume" class="volume form-control" placeholder="Volume">
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="donor_accepted" class="col-sm-10">>1 Venepuncture</label>
                                    <div class="col-sm-2">
                                        <label>
                                            <input type="text" name="venepuncture" id="venepuncture" class="Venepuncture form-control" placeholder="Venepuncture">
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="donor_accepted" class="col-sm-6">Hematoma</label>
                                    <div class="col-sm-3">                                        
                                        <label>Yes 
                                            <input type="radio" class="flat-red" name="donor">
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>No
                                            <input type="radio" class="flat-red" name="donor">
                                        </label>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label for="donor_accepted" class="col-sm-10">Faint</label>
                                    <div class="col-sm-2">
                                        <select name="donor_status" id="donor_status" class="form-control R10">
                                            <option value="">Please select : </option>
                                            <option value="accepted">Mild</option>
                                            <option value="rejected">Moderate</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donor_accepted" class="col-sm-10">Donor Status</label>
                                    <div class="col-sm-2">
                                        <select name="donor_status" id="donor_status" class="form-control R10">
                                            <option value="">Please Select : </option>
                                            <option value="accepted">Accepted</option>
                                            <option value="rejected">Rejected</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="donor_accepted" class="col-sm-2">Report</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" id="report" name="report" rows="1" placeholder="Report">
                                        </textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="donor_accepted" class="col-sm-2">Interviewer</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" id="report" name="report" rows="1" placeholder="Interviewer">
                                        </textarea>
                                    </div>
                                </div>  



                            </div>
                            <div class="box-footer divider">                                        
                                <button type="submit" class="btn btn-primary btn-sm pull-right ion-checkmark">Donate</button>
                                <button type="button" class="btn btn-danger btn-sm pull-left ion-minus-circled">Cancel</button>
                            </div><!-- /.box-footer -->
                    </div><!-- /.box-body -->

                    </form>
            </div><!-- /.box -->
            <!-- general form elements disabled -->


            <!-- /.box-body -->
        </div>

    </div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.1.0
    </div>
    <strong>Copyright &copy; 2016<a href=""></a>.</strong> All rights reserved.
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <!-- Home tab content -->

        <!-- Stats tab content -->
        <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div><!-- /.tab-pane -->
        <!-- Settings tab content -->

    </div>
</aside><!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>

<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>

<script>
    $(document).ready(function () {
        $('#enroll_new_donor_form').submit(function (event) {
            dataString = $("#enroll_new_donor_form").serialize();
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>home/enroll_new_donor",
                data: dataString,
                success: function (data) {


                }

            });
            event.preventDefault();
            return false;
        });


        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });




    });
</script>
</body>
</html>
