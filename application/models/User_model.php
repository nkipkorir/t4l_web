<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class User_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function add_user() {
        $this->db->trans_start();
        $user_name = $this->input->post('user_name');
        $fname = $this->input->post('fname');
        $sname = $this->input->post('sname');
        $national_id = $this->input->post('national_id');
        $password = $this->cryptPass($national_id);
        $date_added = date("Y-m-d H:i:s");
        $data_insert = array(
            'username' => $user_name,
            'fname' => $fname,
            'sname' => $sname,
            'national_id' => $national_id,
            'password' => $password,
            'date_added' => $date_added
        );
        $this->db->insert('users', $data_insert);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function cryptPass($input, $rounds = 9) {
        $salt = "";
        $saltChars = array_merge(range('A', 'Z'), range('a', 'z'), range(0, 9));
        for ($i = 0; $i < 22; $i++):
            $salt.=$saltChars[array_rand($saltChars)];
        endfor;
        return crypt($input, sprintf("$2y$%02d$", $rounds) . $salt);
    }

    function get_user_info($id) {
        return $this->db->get_where('users', array('id' => $id))->result_array();
    }

    function edit_user() {
        $this->db->trans_start();
        $user_id = $this->input->post('id');
        $fname = $this->input->post('fname');
        $sname = $this->input->post('sname');
        $national_id = $this->input->post('national_id');
        $email = $this->input->post('email');
        $status = $this->input->post('status');
        $username = $this->input->post('username');
        echo 'Data passed : ' . $user_id . $fname . $sname . $national_id . $email . $status . $user_id . '<br>';
        $data_update = array(
            'fname' => $fname,
            'sname' => $sname,
            'national_id' => $national_id,
            'email' => $email,
            'status' => $status,
            'username' => $username
        );
        $this->db->where('id', $user_id);
        $this->db->update('users', $data_update);
        $this->db->trans_complete();
        if ($this->db->trans_status() == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    
    
       function delete_users() {
        $this->db->trans_start();
        $id = $this->input->post('id');
        $data_delete = array(
            'status' => 'In Active'
        );
        $this->db->where('id', $id);
        $this->db->update('users', $data_delete);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
