<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DBCentral extends CI_Model {

    function getData() {
        return $this->db->get('donor')->result_array();
    }

    function getUsers() {
        return $this->db->get('users')->result_array();
    }

    function getCounties() {
        return $this->db->get('county')->result_array();
    }

    function get_recruiters() {
        return $this->db->get('recruiter')->result_array();
    }

    function enroll_new_donor() {

        $this->db->trans_start();

        $surname = $this->input->post('surname');
        $othername = $this->input->post('othernames');
        $id_no = $this->input->post('national_id');
        $gender = $this->input->post('gender');
        $blood_group = $this->input->post('blood_group');
        $postal_address = $this->input->post('contact_details');
        $cell_phone = $this->input->post('cell_phone');
        $home_phone = $this->input->post('home_phone');
        $dob = $this->input->post('dob');
        $email = $this->input->post('email');
        $residence_county = $this->input->post('county_residence');
        $education_level = $this->input->post('education_level');
        $marital_status = $this->input->post('marital_status');
        $recruiter = $this->input->post('recruiter');
        $other_mobilizer_partner = $this->input->post('other_recruiter');
        $consent = $this->input->post('accept_consent');
        $user_id = "1";
        $date_added = date("Y-m-d H:i:s");


        $data_insert = array(
            'sname' => $surname,
            'oname' => $othername,
            'gender' => $gender,
            'id_no' => $id_no,
            'marital_status' => $marital_status,
            'dob' => $dob,
            'address' => $postal_address,
            'home_no' => $home_phone,
            'cell_no' => $cell_phone,
            'email' => $email,
            'residence' => $residence_county,
            'education_level' => $education_level,
            'recruiter' => $recruiter,
            'blood_group' => $blood_group,
            'date_added' => $date_added
        );
        $this->db->insert('donor', $data_insert);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
   public function select()  
      {  
         //data is retrive from this query  
         $query = $this->db->get('stock');  
         return $query;  
      }  

    function check_auth() {
//        $this->db->trans_start();
        $username = $this->input->post('username');
        $password = $this->input->post('password');


        if (filter_var($username, FILTER_VALIDATE_EMAIL)) {

            $check_existence = $this->db->get_where('users', array('email' => $username))->num_rows();
            $get_user_pass = $this->db->get_where('users', array('email' => $username));
        } else {
            echo 'Posted values : Username :  ' . $username . 'Password : ' . $password . '<br>';
            $check_existence = $this->db->get_where('users', array('username' => $username))->num_rows();
            $get_user_pass = $this->db->get_where('users', array('username' => $username))->result_array();
        }
       

        if ($check_existence >= 1) {
            foreach ($get_user_pass as $value) {
                $pass = $value['password'];
                $crypted_pass = crypt($password, $value['password']);
                if ($crypted_pass === $pass) {
                    return 'Login success';
                } else {
                    return 'Wrong password...';
                }
            }
        } else {
            return'User does not exist...';
        }




//        $this->db->trans_complete();
//        if ($this->db->trans_status() === FALSE) {
//            return TRUE;
//        } else {
//            return FALSE;
//        }
    }

    function check_username($username) {
        return $this->db->get_where('users', array('username' => $username))->result_array();
    }

    function check_email($username) {
        return $this->db->get_where('users', array('email' => $username))->result_array();
    }

}
