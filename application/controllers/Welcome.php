<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        $this->load->view('welcome_message');
    }

    function general() {
        $this->load->view('pages/forms/general');
    }

    function advanced() {
        $this->load->view('pages/forms/advanced');
    }

    function editors() {
        $this->load->view('pages/forms/editors');
    }
    
    function enroll_donor() {
        $surname = $this->input->post('');
        $othername = $this->input->post('');
    }
//    function datatables(){
//        $this->load->view('pages/tables/data');
//    }
//    
     public function datatables()  
      {  
//         //load the database  
//         $this->load->database();  
//         //load the model  
//         $this->load->model('DBCentral');  
         //load the method of model  
         $data['h']=$this->DBCentral->select();  
         //return the data in view  
         $this->load->view('pages/tables/data', $data);  
      }  

}
