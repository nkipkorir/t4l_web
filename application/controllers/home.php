<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Home extends MY_Controller {

    public $data = '';
    public $county = '';
    public $users = '';

    function __construct() {
        parent::__construct();
        $this->data = new DBCentral();
        $this->county = new County_model();
        $this->users = new User_model();
    }

    function index() {
        $this->load->view('form');
    }

    function enroll_new_donor() {
        $donor_enrollment = $this->data->enroll_new_donor();
        if ($donor_enrollment) {
            return'Success';
        } else {
            return 'Fail';
        }
    }

    function counties() {
        $counties = $this->data->getCounties();
        $third_uri = $this->uri->segment(3);
        if (empty($third_uri) && $third_uri !== "json") {
            $data["counties"] = $counties;
            $view = "county_v";
            $this->load->view($view, $data);
        } else {
            echo json_encode($counties);
        }
    }

    function recruiters() {
        $third_uri = $this->uri->segment(3);
        $recruiters = $this->data->get_recruiters();
        if (empty($third_uri) && $third_uri !== "json") {
            $data['recruiters'] = $recruiters;
            $view = "recruiters_v";
            $this->load->view($view, $data);
        } else {
            echo json_encode($recruiters);
        }
    }

    function add_county() {
        $add_county = $this->county->add_county();
        if ($add_county) {
            return 'Success';
        } else {
            return 'Fail';
        }
    }

    function get_county_info() {
        $id = $this->uri->segment(3);
        $get_county_info = $this->county->get_county_info($id);
        echo json_encode($get_county_info);
    }

    function edit_county() {
        $edit_county = $this->county->edit_county();
        if ($edit_county) {
            return 'Success';
        } else {
            return 'Fail';
        }
    }

    function delete_county() {
        $delete_county = $this->county->delete_county();
        if ($delete_county) {
            return 'Success';
        } else {
            return 'Fail';
        }
    }

    function bulk_add_counties() {
        $objPHPExcel = new PHPExcel_Reader_CSV();
    }

    function users() {
        $users = $this->data->getUsers();
        $third_uri = $this->uri->segment(3);
        if (empty($third_uri) && $third_uri !== "json") {
            $data["users"] = $users;
            $view = "users_v";
            $this->load->view($view, $data);
        } else {
            echo json_encode($users);
        }
    }

    function add_user() {
        $user = $this->users->add_user();
        if ($user) {
            return 'Success';
        } else {
            return 'Fail';
        }
    }

    function get_user_info() {
        $id = $this->uri->segment(3);
        $user_info = $this->users->get_user_info($id);
        if (empty($user_info)) {
            echo json_encode($user_info);
        } else {
            echo json_encode($user_info);
        }
    }

    function edit_user() {
        $edit_user = $this->users->edit_user();
        if ($edit_user) {
            return 'Success';
        } else {
            return 'Fail';
        }
    }

    function delete_user() {
        $delete_user = $this->users->delete_users();
        if ($delete_user) {
            return 'Success';
        } else {
            return 'Fail';
        }
    }

    function login() {
        $view = 'Login';
        $this->load->view($view);
    }

    function check_auth() {
        $validate_user = $this->data->check_auth();
    }
        

}
